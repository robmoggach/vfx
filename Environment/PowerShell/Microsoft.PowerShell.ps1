# Fix/Set the console size
# $Shell = $Host.UI.RawUI
# $size = $Shell.WindowSize
# $size.width=150
# $size.height=50
# $Shell.WindowSize = $size
$Host.UI.RawUI.WindowTitle = (Get-Date -UFormat '%Y-%m-%d %R').ToString()
Set-PSReadlineOption -EditMode Emacs

# Change how powershell does tab completion
# http://stackoverflow.com/questions/39221953/can-i-make-powershell-tab-complete-show-me-all-options-rather-than-picking-a-sp
Set-PSReadlineKeyHandler -Chord Tab -Function MenuComplete

# Load the Windows Dev Kit into the PATH
$env:Path = $env:Path + ";C:\Program Files (x86)\Windows Kits\8.1\bin\x64"

# Load Python
$env:Path = "C:\Python27\;C:\Python27\Scripts\;C:\Python38\;C:\Python38\Scripts\;" + $env:Path

# Proper history etc
Import-Module PSReadLine

# Produce UTF-8 by default
# https://news.ycombinator.com/item?id=12991690
$PSDefaultParameterValues["Out-File:Encoding"] = "utf8"

# History Limit
# https://technet.microsoft.com/en-us/magazine/hh241048.aspx
$MaximumHistoryCount = 10000;

# Aliases
# Create alias for trash to move to trash
Set-Alias trash Remove-ItemSafely
Set-Alias ll Get-ChildItem
set-alias new-link ln
Set-Alias ll Get-ChildItem
set-alias find find-file
set-alias find-name find-file


# Functions
# create symbolic links
# From https://stackoverflow.com/questions/894430/creating-hard-and-soft-links-using-powershell
function ln($target, $link) {
    New-Item -ItemType SymbolicLink -Path $link -Value $target
}
function touch ([string]$Name) {
    New-Item $Name -ItemType file
}
# uptime!
function uptime {
    Get-CimInstance Win32_OperatingSystem | select-object csname, @{LABEL = 'LastBootUpTime';
        EXPRESSION                                                        = { $_.ConverttoDateTime($_.lastbootuptime) }
    }
}
# df
function df {
    get-volume
}
function find-file($name) {
    get-childitem -recurse -filter "*${name}*" -ErrorAction SilentlyContinue | foreach-object {
        write-output = $PSItem.FullName
    }
}
function open($file) {
    invoke-item $file
}
function explorer {
    explorer.exe .
}
function edge {
    start microsoft-edge:
}
function settings {
    start-process ms-setttings:
}
function chrome {
    start chrome;
}
# Truncate homedir to ~
function limit-HomeDirectory($Path) {
    $Path.Replace("$home", "~")
}

function fix-uncpath($Path) {
    $Path.Replace("Microsoft.PowerShell.Core\FileSystem::", "")
}

# Oddly, Powershell doesn't have an inbuilt variable for the documents directory. So let's make one:
# From https://stackoverflow.com/questions/3492920/is-there-a-system-defined-environment-variable-for-documents-directory
$env:DOCUMENTS = [Environment]::GetFolderPath("mydocuments")

# PS comes preset with 'HKLM' and 'HKCU' drives but is missing HKCR
New-PSDrive -Name HKCR -PSProvider Registry -Root HKEY_CLASSES_ROOT | Out-Null

# Custom Prompt
################

# Changes PowerShell prompt to current folder only (shorter prompt) in lowercase.

function prompt {
    $Host.UI.RawUI.WindowTitle = (Get-Date -UFormat '%Y-%m-%d %R').ToString()
    $PromptString = '[' + (Get-Date -UFormat '%T') + '] ' + $(fix-uncpath($(limit-HomeDirectory("$pwd"))))
    #Write-Host '[' -NoNewline
    #Write-Host (Get-Date -UFormat '%T') -ForegroundColor cyan -NoNewline
    #Write-Host ']:' -NoNewline
    #Write-Host (Split-Path (Get-Location) -Leaf) -NoNewline
    #return "$ "
    Write-Host $PromptString -ForegroundColor Yellow -NoNewline
    return " > "
}

$env:APPS = "\\studio.lan\studio\tools\applications\win64"


function nuke() {
    Param(
        [Parameter(Mandatory = $false)]
        [string] $Variant,
        [Parameter(Mandatory = $false)]
        [int] $Version
    )
    if (!($Variant)) {
        $Variant = 'Normal'
        if (!($Version)) {
            $Version = 11
        }
    }
    Switch ($Version) {
        "11" {
            $env:NUKE_MAJOR_RELEASE = 11
            $env:NUKE_MINOR_RELEASE = 3
            $env:NUKE_BUILD_VERSION = 4
            $env:NUKE_VERSION = $env:NUKE_MAJOR_RELEASE + '.' + $env:NUKE_MINOR_RELEASE + 'v' + $env:NUKE_BUILD_VERSION
            $env:NUKE = $env:APPS + "\Nuke"
            $env:NUKE_PATH = "\\studio.lan\studio\tools\pipeline\apps\nuke\core"
            $env:NB = "C:\Program Files" + "\Nuke" + $env:NUKE_VERSION
            $env:Path = $env:Path + ";" + $env:NB
        }
    }
    Switch ($Variant) {
        "Normal" {
            Write-Host $env:NB
            Start-Process Nuke11.3 -Wait -NoNewWindow 
        }
        "x" {
            Write-Host $env:NB
            Start-Process Nuke11.3 -Wait -NoNewWindow -ArgumentList ('--nukex')
        }
        "studio" {
            Write-Host $env:NB
            Start-Process Nuke11.3 -Wait -NoNewWindow -ArgumentList ('--studio')
        }
    }
}
function houdini16 {
    $env:HFS="\\studio\studio_tools\applications\win64\Houdini\16.5.496"
    $env:HB=$env:HFS + "\bin"
    $env:HD=$env:HFS + "\demo"
    $env:HH=$env:HFS + "\houdini"
    $env:HHC=$env:HFS + "\config"
    $env:HT=$env:HFS + "\toolkit"
    $env:Path = $env:Path + ";" + $env:HB
    $env:HOUDINI_MAJOR_RELEASE=16
    $env:HOUDINI_MINOR_RELEASE=5
    $env:HOUDINI_BUILD_VERSION=634
    $env:HOUDINI_VERSION=$env:HOUDINI_MAJOR_RELEASE + '.' + $env:HOUDINI_MINOR_RELEASE + '.' + $env:HOUDINI_BUILD_VERSION
}

function houdini17 {
    $env:HFS="\\studio\studio_tools\applications\win64\Houdini\17.0.459"
    $env:HB=$env:HFS + "\bin"
    $env:HD=$env:HFS + "\demo"
    $env:HH=$env:HFS + "\houdini"
    $env:HHC=$env:HFS + "\config"
    $env:HT=$env:HFS + "\toolkit"
    $env:Path = $env:Path + ";" + $env:HB
    $env:HOUDINI_MAJOR_RELEASE=17
    $env:HOUDINI_MINOR_RELEASE=0
    $env:HOUDINI_BUILD_VERSION=459
    $env:HOUDINI_VERSION=$env:HOUDINI_MAJOR_RELEASE + '.' + $env:HOUDINI_MINOR_RELEASE + '.' + $env:HOUDINI_BUILD_VERSION
}


function hou() {
    [CmdletBinding(DefaultParameterSetName = 'Variant')]
    Param
    (
        [Parameter(Mandatory = $false, Position = 1, ParameterSetName = 'Variant')]
        [string] $Variant,
        [Parameter(Mandatory = $false, Position = 2, ParameterSetName = 'Version')]
        [decimal] $Version
    )
    $Version = 17.5
    $env:HOUDINI = $env:APPS + "\Houdini"
    $env:HOUDINI = "C:\Program Files\Side Effects Software"
    if (!($Variant)) {
        $Variant = 'Indie'
        if (!($Version)) {
            $Version = "17.5"
        }
    }
    Switch ($Version) {
        "17" {
            $env:HOUDINI_MAJOR_RELEASE = 17
            $env:HOUDINI_MINOR_RELEASE = 0
            $env:HOUDINI_BUILD_VERSION = 459
        }
        "17.5" {
            $env:HOUDINI_MAJOR_RELEASE = 17
            $env:HOUDINI_MINOR_RELEASE = 5
            $env:HOUDINI_BUILD_VERSION = 173
        }
    }
    $env:HOUDINI_VERSION = $env:HOUDINI_MAJOR_RELEASE + '.' + $env:HOUDINI_MINOR_RELEASE + '.' + $env:HOUDINI_BUILD_VERSION
    $env:HFS = $env:HOUDINI + "\Houdini " + $env:HOUDINI_VERSION 
    $env:H = $env:HFS
    $env:HH = $env:H + "\houdini"
    $env:HHC = $env:HH + "\config"
    $env:HT = $env:H + "\toolkit" 
    $env:HB = $env:H + "\bin"
    $env:HD = $env:HH + "\demo"
    $env:HHC = $env:HH + "\config"
    $env:HT = $env:HH + "\toolkit"
    $env:HPY = $env:H + "\python27"
    $env:Path = $env:HPY + ";" + $env:Path
    $env:LD_LIBRARY_PATH = $env:HPY + "\lib\site-packages;" + $env:LD_LIBRARY_PATH
    $env:PYTHONPATH = $env:HPY + "\lib\site-packages;" + $env:PYTHONPATH
    $env:Path = $env:Path + ";" + $env:HB
    Switch ($Variant) {
        "Indie" {
            Write-Host $env:HB
            Start-Process hindie -Wait -NoNewWindow 
            start hindie
        }
        "core" {
            Write-Host $env:HB
            Start-Process houdinicore -Wait -NoNewWindow 
        }
        "fx" {
            Write-Host $env:HB
            Start-Process houdinifx -Wait -NoNewWindow 
        }
    }

}

# Functions
# Load git from Github for Windows
# . (Resolve-Path "$env:LOCALAPPDATA\GitHub\shell.ps1")


## Set the atom root Folder
#$atomRoot = "$env:LOCALAPPDATA\atom"
## List all folders in the atomRoot and set the highest version to 0.0.0
#$atomVersions = Get-ChildItem -Path $atomRoot | ? { $_.PSIsContainer } | Select-Object FullName
#$atomVersionParts = 0, 0, 0
## Loop through each folder, if it is higher than the highest version update it.
#forEach ($versionFolder in $atomVersions) {
#    $folderName = ($versionFolder.FullName -split '\\')[-1]
#    if ($folderName -match '^app') {
#        $version = $folderName -replace '^app-', ''
#        $versionParts = $version -split '\.'
#        if ($versionParts[0] -ge $atomVersionParts[0]) {
#            $atomVersionParts[0] = $versionParts[0]
#            $atomVersionParts[1] = $versionParts[1]
#            $atomVersionParts[2] = $versionParts[2]
#            if ($versionParts[1] -ge $atomVersionParts[1]) {
#                $atomVersionParts[1] = $versionParts[1]
#                $atomVersionParts[2] = $versionParts[2]
#                if ($versionParts[2] -ge $atomVersionParts[2]) {
#                    $atomVersionParts[2] = $versionParts[2]
#                }
#            }
#        }
#    }
#}
## Set the path to the latest atom version
#$atomVersion = $atomVersionParts -join '.'
#$atomPath = $atomRoot + "\app-" + $atomVersion
#
## Set the aliases
#New-Item alias:atom -value ($atomPath + '\resources\cli\atom.cmd')
#New-Item alias:apm -Value ($atomPath + '\resources\app\apm\bin\apm.cmd')
#
## Clear-Host
#$nodeVersion = node -v
#Write-Host ("Atom: " + $atomVersion)
#Write-Host ("Node: " + $nodeVersion)

# Start the SSH Agent
cmd /c start-ssh-agent.cmd

# Import-Module posh-git
# Import-Module oh-my-posh
# Set-Theme Paradox
