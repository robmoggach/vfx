@echo off

set Houdini_ver=%1


:: Check for existing license server
if EXIST C:\windows\system32\hserver.exe goto SkippedServerSetup


:: Copy the service files
copy \\studio\tools\applications\win64\Houdini\%Houdini_ver%\hserver.exe C:\windows\system32\
copy \\studio\tools\applications\win64\Houdini\%Houdini_ver%\hserver.ini C:\windows\system32\


:: Create the license service
sc create HoudiniServer binPath= C:\windows\system32\hserver.exe start=auto
:: Workaround for "timeout" not working in a background batch-script - wait 5 sec
ping 127.0.0.1 -n1 -w 50000 >NUL


:SkippedServerSetup

:: Make sure the license service is running
sc start HoudiniServer
:: Workaround for "timeout" not working in a background batch-script - wait 5 sec
ping 127.0.0.1 -n1 -w 5000 >NUL

:: Set the license server
C:\windows\system32\hserver -S license3


:: Copy the shortcuts to the start menu
robocopy "\\studio\tools\applications\win64\Houdini\%Houdini_ver%\_shortcuts" "C:\ProgramData\Microsoft\Windows\Start Menu\Programs\Side Effects Software\Houdini %Houdini_ver%" /E /MT

